package com.z.xu.callauthenticator

import android.app.Application
import timber.log.Timber



/**
 * Created by kyo on 2017/08/08.
 */
class MainApp: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.uprootAll()
            Timber.plant(Timber.DebugTree())
        }
    }
}